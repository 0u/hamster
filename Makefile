all: release

upx:
	upx --brute bin/*

wintest:
	GOOS=windows go build -o bin/svchost.exe -ldflags="-s -w"

release:
	mkdir -p ./bin
	GOOS=windows go build -o bin/svchost.exe -ldflags="-s -w -H=windowsgui"
	GOOS=linux   go build -o bin/linux.exe   -ldflags="-s -w"
	GOOS=darwin  go build -o bin/darwin.exe  -ldflags="-s -w"
